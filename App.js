import { StyleSheet, View, FlatList, Button ,Keyboard} from "react-native";
import React, { useState } from "react";
import GoalItem from "./components/GoalItem";
import GoalInptComponent from "./components/GoalInptComponent";

const App = () => {
  const [CourseGoals, setCourseGoals] = useState([]);
  const [ModalIsVisible,SetModalIsVisible]=useState(false)

  function StartAddGoalHandler()
  {
    SetModalIsVisible(true)
  }
  function EndGoalHandler(){
    SetModalIsVisible(false)
  }
  function AddGoalHandler(EnteredCourseGoals) {
    setCourseGoals((currentCourseGoals) => [
      ...currentCourseGoals,
      { text: EnteredCourseGoals, id: Math.random().toString() },
    ]);
    //best way of state updating like list
    EndGoalHandler() // or setModalIsVisible(false)
  }
  function DeleteGoalHandler(id) {
    setCourseGoals((currentCourseGoals) => {
      return currentCourseGoals.filter((goal) => goal.id !== id);
    });
  }

  return (
    <View style={styles.AppContainer}>
      <Button title="add new goal"
       color="black" onPress={StartAddGoalHandler} />
      <GoalInptComponent onButtonPress={AddGoalHandler} visible={ModalIsVisible} onCancel={EndGoalHandler} />
      <View style={styles.Goalscontainer}>
        <FlatList
          data={CourseGoals}
          renderItem={(itemData) => {
            return (
              <GoalItem
                label={itemData.item.text}
                onDelete={DeleteGoalHandler}
                id={itemData.item.id}
              />
            );
          }}
          keyExtractor={(item, index) => {
            return item.id;
          }}
        />
      </View>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  AppContainer: {
    paddingTop: 50,
    paddingHorizontal: 16,
    flex: 1,
  },

  Goalscontainer: {
    flex: 4,
  },
});
