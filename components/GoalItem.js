import { StyleSheet, Text, View, Pressable } from "react-native";
import React from "react";

const GoalItem = (props) => {
  return (
    <View style={styles.GoalItem}>
      <Pressable
        onPress={props.onDelete.bind(this, props.id)}
        android_ripple={{ color: "white" }}
      >
        <Text style={styles.TextStyle}>{props.label}</Text>
      </Pressable>
    </View>
  );
};

export default GoalItem;

const styles = StyleSheet.create({
  GoalItem: {
    margin: 8,
    borderRadius: 6,
    backgroundColor: "cyan",
    elevation:5
  },
  TextStyle: {
    fontSize: 20,
    fontWeight: "bold",
    padding: 5,
    padding: 20,

  },
});
