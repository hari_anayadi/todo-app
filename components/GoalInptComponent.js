import { StyleSheet, Text, View, TextInput, Button, Modal,Keyboard } from "react-native";
import React, { useState } from "react";

const GoalInptComponent = (props) => {
  const [EnteredCourseGoals, setEnteredCourseGoals] = useState("");

  function GoalInputHandler(goals) {
    setEnteredCourseGoals(goals);
  }

  function AddGoalHandler() {
    props.onButtonPress(EnteredCourseGoals);
    setEnteredCourseGoals("");
  }
  return (
    <Modal visible={props.visible} animationType="fade">
      <View style={styles.TextInputContainer}>
        <TextInput
          placeholder="enter your task"
          style={styles.TextInput}
          onChangeText={GoalInputHandler}
          value={EnteredCourseGoals}
        />
        <View style={styles.ButtonContainer}>
          <View style={styles.Button}>
            <Button title="Add task" onPress={AddGoalHandler} color={"green"}/>
          </View>
          <View style={styles.Button}>
            <Button title="cancel" onPress={props.onCancel} color="red"/>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default GoalInptComponent;

const styles = StyleSheet.create({
  TextInputContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 24,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
    flex: 1,
    padding:16
  },
  TextInput: {
    borderWidth: 1,
    padding: 12,
    width: "100%",
    borderColor: "#cccccc",
  },
  ButtonContainer: {
    flexDirection: "row",
    marginTop:16
  },
  Button: {
      width:100,
      marginHorizontal:8
  },
});
